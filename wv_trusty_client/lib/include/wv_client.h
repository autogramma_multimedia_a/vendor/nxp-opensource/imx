#pragma once
#include <trusty/tipc.h>

__BEGIN_DECLS
void set_secure_pipe(int enable);
__END_DECLS
